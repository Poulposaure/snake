#include <stdio.h>
#include <stdlib.h>
#include "gestion_clavier.h"
#include "couleur_console.h"
#include "player_score.h"
#include "table_score.h"

/******************************************/
/*                OPTIONS                 */
/******************************************/

// STORAGE OF SCORES AND PLAYERS NAME
#define CHAINED_LIST 0
#define ARRAY 1

// CHANGE THE GRID SIZE DURING THE GAME
#define DYNAMIC_GRID 1

// CHANGE THE MOVE KEYS DURING THE GAME
#define DYNAMIC_MOVES 0
#define STATIC_MOVES 1


#define TITLE_FILE "title.txt"
#define MAX_STRING 100

/****************************************************/
/*                                                  */
/*                                                  */
/*                                                  */
/****************************************************/

typedef struct Coordinate
{
    unsigned int r;// row
    unsigned int c;// column
} Coordinate;


typedef struct Snake
{
    unsigned int number;
    Coordinate pos[(WIDTH - 1) * (LENGTH - 1)];
} Snake;

void display_title();
void menu();

void game(TableScore *table_score);
void game_clv(PlayerScore **player_score);

void init_grid(char ***grid);
void fill_grid(char **grid);
void display_play_area(char **grid);
int collision(char **g, Snake *s);
int update_grid(char **grid, Snake *snake, char move);

void init_snake(Snake *snake);
void move(Snake *snake, char key);
void display_current_score(Snake *snake);

int item_on_snake(Coordinate *item, Snake *snake);
int moving_backwards(char move, char last_move);
void random_coordinates(Coordinate *c, char **grid, char item);

void options();
void input_grid_dimensions();
void input_size(unsigned int *size);
void input_delay();
void input_move_keys();
void input_move(char *move, char buffer_move_keys[5], int index);




/******************************************/
/*            GLOBAL VARIABLES            */
/******************************************/

#if DYNAMIC_MOVES
char g_left = 's', g_right = 'f', g_top = 'e', g_bottom = 'd';
#endif

long g_nano_delay = 100000000;
unsigned int g_length = 30, g_width = 30;


int main(int argc, char *argv[])
{
    CLEAR
    display_title();
    menu();

    return 0;
}

void menu()
{
    int choice = 0;

    #if CHAINED_LIST
    PlayerScore *player_score = get_scores_clv();
    #endif
    #if ARRAY
    TableScore table_score;
    init_table_score(&table_score);
    alloc_table_score(&table_score);
    get_scores(&table_score);
    #endif

    // we display the menu and the options
    while(1)
    {
        puts("\n 1 - Jouer");
        puts(" 2 - Consulter les 5 meilleurs scores");
        puts(" 3 - Options");
        puts(" 0 - Quitter\n\n");
        printf("Joueur$ ");

        scanf(" %1d", &choice);

        switch(choice)
        {
            case 1:
                #if CHAINED_LIST
                game_clv(&player_score);
                #endif
                #if ARRAY
                game(&table_score);
                #endif

                break;
            case 2:
                #if CHAINED_LIST
                best_scores_clv(player_score);
                #endif
                #if ARRAY
                best_scores(&table_score);
                #endif

                break;
            case 3:
                options();

                break;
            case 0:
                puts("\n\n\nMerci d'avoir joue !\n");

                #if CHAINED_LIST
                save_score_clv(player_score);
                free_players_score(&player_score);
                #endif
                #if ARRAY
                save_score(&table_score);
                #endif

                return;
            default:
                break;
        }// switch
        CLEAR
    }// while

    return;
}

void display_title()
{
    FILE *title_file = fopen(TITLE_FILE, "r");
    char string[MAX_STRING] = "";

    if (title_file == NULL) return;

    while (!feof(title_file))
    {
        fgets(string, MAX_STRING, title_file);
        printf("%s", string);
    }
    fclose(title_file);

    return;
}


void game_clv(PlayerScore **player_score)
{
    Snake snake;
    Coordinate food, head_position;
    char key, last_move, **grid;
    int a_collision;

    init_grid(&grid);
    fill_grid(grid);
    init_snake(&snake);
    random_coordinates(&food, grid, FOOD);
    random_coordinates(&head_position, grid, HEAD);
    snake.pos[0] = head_position;

    // the player must input a key, while he is not losing, nor winning
    // nor pressing the key 'q'
    do
    {
        key = pressing_key(g_nano_delay);

        // if the player did not input anything, the snake redo the last move
        if(key == NO_KEY || moving_backwards(key, last_move))
        {
            a_collision = update_grid(grid, &snake, last_move);
        }
        else
        {
            a_collision = update_grid(grid, &snake, key);
            last_move = key;
        }

        // if there is a collision with a wall or with his own body
        if (a_collision || snake.number == 0)
        {
            //afficher_texte("\n\tGame over\n\n\n\n", ROUGE, NOIR, GRAS);
            puts("\n\tGame over\n\n\n");
            getchar();
            input_score_clv(player_score, ((snake.number != 0) ? snake.number - 1 : 0));
            break;
        }
        display_play_area(grid);
        display_current_score(&snake);

    } while (key != QUIT);

    return;
}

void game(TableScore *table_score)
{
    Snake snake;
    Coordinate food;
    Coordinate head_position;
    char key, last_move, **grid;
    int a_collision;
    init_grid(&grid);
    fill_grid(grid);
    init_snake(&snake);
    random_coordinates(&food, grid, FOOD);
    random_coordinates(&head_position, grid, HEAD);
    snake.pos[0] = head_position;

    // the player must input a key, while he is not losing, nor winning
    // nor pressing the key 'q'
    do
    {
        key = pressing_key(g_nano_delay);

        // if the player did not input anything, the snake redo the last move
        if(key == NO_KEY || moving_backwards(key, last_move))
        {
            a_collision = update_grid(grid, &snake, last_move);
        }
        else
        {
            a_collision = update_grid(grid, &snake, key);
            last_move = key;
        }

        // if there is a collision with a wall or with his own body
        if (a_collision || snake.number == 0)
        {
            //afficher_texte("\n\tGame over\n\n\n\n", ROUGE, NOIR, GRAS);
            puts("\n\tGame over\n\n\n");
            getchar();
            input_score(table_score, ((snake.number != 0) ? snake.number - 1 : 0));
            break;
        }
        display_play_area(grid);
        display_current_score(&snake);

    } while (key != QUIT);


    return;
}

void init_grid(char ***grid)
{
    int ligne;

    *grid = (char **) malloc(g_length * sizeof(char *));
    for (ligne = 0; ligne < g_length; ligne++)
    {
        (*grid)[ligne] = (char *) malloc(g_width * sizeof(char));
    }

    return;
}

void fill_grid(char **grid)
{
    int row, col;

    // we put the walls vertically
    for(row = 0; row != g_length; row++)
    {
        grid[row][0] = WALL;
        grid[row][g_width - 1] = WALL;
    }

    // we put the walls horizontally
    for(col = 0; col != g_width; col++)
    {
        grid[0][col] = WALL;
        grid[g_length - 1][col] = WALL;
    }

    // we put everywhere else spaces
    for(row = 1; row != g_length - 1; row++)
    {
        for(col = 1; col != g_width - 1; col++)
        {
            grid[row][col] = SPACE;
        }
    }

    return;
}

void init_snake(Snake *snake)
{
    snake->number = 1;

    return;
}




int moving_backwards(char move, char last_move)
{
    // we check if the player has input the opposite of his last move
    #if STATIC_MOVES
    if (move == RIGHT && last_move == LEFT) return 1;
    if (move == LEFT && last_move == RIGHT) return 1;
    if (move == BOTTOM && last_move == TOP) return 1;
    if (move == TOP && last_move == BOTTOM) return 1;
    #endif
    #if DYNAMIC_MOVES
    if (move == g_right && last_move == g_left) return 1;
    if (move == g_left && last_move == g_right) return 1;
    if (move == g_bottom && last_move == g_top) return 1;
    if (move == g_top && last_move == g_bottom) return 1;
    #endif

    return 0;
}


void move(Snake *snake, char move)
{
    int body;
    // we move the body of the snake; each part must take the place of the part
    // which is ahead; simply put: follow
    for (body = snake->number + 1; body != 0; body--)
    {
        snake->pos[body] = snake->pos[body - 1];
    }

    // we move the snake accordingly to the player's input
    #if STATIC_MOVES
    switch (move)
    {
        case LEFT:
            (snake->pos[0].c)--;
            break;
        case RIGHT:
            (snake->pos[0].c)++;
            break;
        case TOP:
            (snake->pos[0].r)--;
            break;
        case BOTTOM:
            (snake->pos[0].r)++;
            break;
    }
    #endif
    #if DYNAMIC_MOVES
    if (move == g_left) (snake->pos[0].c)--;
    if (move == g_right) (snake->pos[0].c)++;
    if (move == g_top) (snake->pos[0].r)--;
    if (move == g_bottom) (snake->pos[0].r)++;
    #endif

    return;
}


int update_grid(char **grid, Snake *snake, char key)
{
    int num, a_collision = 1;
    Coordinate item;

    // the last part of the snake is disapearing, the other parts will be
    // overwritten
    num = snake->number - 1;
    grid[snake->pos[num].r][snake->pos[num].c] = ' ';

    move(snake, key);

    // we check if the snake has eaten the food
    if (grid[snake->pos[0].r][snake->pos[0].c] == FOOD)
    {
        // if so, we put another food in the grid
        random_coordinates(&item, grid, FOOD);
        snake->number++;// the body of the snake is increasing

        // we add an obstacle
        if (snake->number%(DIFFICULTY) == DIFFICULTY - 1)
        {
            random_coordinates(&item, grid, WALL);
        }
    }

    // we check if the snake has reached a wall or an obstacle
    a_collision = collision(grid, snake);

    // we display the whole body of the snake
    grid[snake->pos[0].r][snake->pos[0].c] = HEAD;
    for (num = 1; num != snake->number; num++)
    {
        grid[snake->pos[num].r][snake->pos[num].c] = BODY;
    }

    return a_collision;
}


int item_on_snake(Coordinate *item, Snake *snake)
{
    int num;

    // we check for every body of the snake if the coordinates of the food
    // are the same
    for (num = 0; num != snake->number; num++)
    {
        if (snake->pos[num].r == item->r && snake->pos[num].c == item->c)
        {
            return 1;
        }
    }

    return 0;
}


int collision(char **g, Snake *s)
{
    char cell = g[s->pos[0].r][s->pos[0].c];
    return cell == WALL || cell == BODY || cell == SPIKE;
}


void display_play_area(char **grid)
{
    int row, col;

    CLEAR// we clear the console
    for(row = 0; row != g_length; row++)
    {
        for(col = 0; col != g_width; col++)
        {
            printf("%c", grid[row][col]);
        }
        printf("\n");
    }

    return;
}


void display_current_score(Snake *snake)
{
    // the current score of the player is the length of the body/snake
    printf("\tScore : %d\n", snake->number - 1);
    // the keys needed the move the snake are printed
    #if STATIC_MOVES
    printf("\n\t^\n\t%c\n%c <\t\t> %c\n\t%c\n\tv\n", TOP, LEFT, RIGHT, BOTTOM);
    #endif
    #if DYNAMIC_MOVES
    printf("\n\t^\n\t%c\n%c <\t\t> %c\n\t%c\n\tv\n", g_top, g_left, g_right, g_bottom);
    #endif

    return;
}

void random_coordinates(Coordinate *c, char **grid, char item)
{
    // the coordinates must be inside the play area, but not on the walls
    do
    {
        do
        {
            c->r = (rand() % g_length) + 1;
            c->c = (rand() % g_width) + 1;
        } while (c->r >= g_length - 1 || c->c >= g_width - 1);
    } while (grid[c->r][c->c] != SPACE);

    grid[c->r][c->c] = item;

    return;
}



void options()
{
    int choice;

    while (1)
    {
        CLEAR
        puts("\nOptions\n");
        puts("  1 - Modifier les dimensions de la grille");
        puts("  2 - Modifier la rapidite du jeu");
        puts("  3 - Modifier les touches du jeu");
        puts("  0 - Quitter\n");
        printf("Joueur$ ");

        scanf(" %d", &choice);

        switch (choice)
        {
            case 1:
                #if DYNAMIC_GRID
                input_grid_dimensions();
                #endif
                break;
            case 2:
                input_delay();
                break;
            case 3:
                #if DYNAMIC_MOVES
                input_move_keys();
                #endif
                break;
            case 0:
                return;
        }
    }

    return;
}

void input_grid_dimensions()
{
    CLEAR
    puts("Dimensions de la grille\n");

    puts("   Largeur de la zone de jeu\n");
    printf("Joueur$ ");
    input_size(&g_width);

    puts("   Longueur de la zone de jeu\n");
    printf("Joueur$ ");
    input_size(&g_length);

    return;
}

void input_size(unsigned int *size)
{
    do
    {
        scanf(" %d", size);
    } while (*size <= 0);

    return;
}


void input_delay()
{
    int choice;

    puts("\nRapidite du jeu\n");
    puts("  1 - RAPIDE\n  2 - PAS RAPIDE\n  3 - LENT\n\n  4 - TRES LENT\n");
    printf("Joueur$ ");

    scanf(" %d", &choice);

    switch (choice)
    {
        case 1:
            g_nano_delay = 70000000;
            break;
        case 2:
            g_nano_delay = 100000000;
            break;
        case 3:
            g_nano_delay = 200000000;
            break;
        case 4:
            g_nano_delay = 300000000;
            break;
    }

    return;
}


void input_move_keys()
{
    #if DYNAMIC_MOVES
    char buffer_move_keys[5] = "    ";

    puts("\nTOUCHES DU JEU\n");
    printf("   HAUT : ");
    scanf(" %c", buffer_move_keys+0);

    input_move("BAS", buffer_move_keys, 1);
    input_move("DROITE", buffer_move_keys, 2);
    input_move("GAUCHE", buffer_move_keys, 3);

    g_top = buffer_move_keys[0];
    g_bottom = buffer_move_keys[1];
    g_right = buffer_move_keys[2];
    g_left = buffer_move_keys[3];
    #endif

    return;
}


void input_move(char move[10], char buffer_move_keys[5], int index)
{
    char input;

    // each time the user input a char, we have to check if it is already in
    // the array; to do that we search the occurence of the char in the array
    // the funtion strchr returns NULL if it didn't find any
    do
    {
        printf("\n   %s : ", move);
        scanf(" %c", &input);
    } while (strchr(buffer_move_keys, input) != NULL);
    *(buffer_move_keys+index) = input;

    return;
}
