#ifndef PLAYER_SCORE
#define PLAYER_SCORE



#include <stdio.h>
#include <stdlib.h>
#include <string.h>


/******************************************/
/*               CONSTANTES               */
/******************************************/

#define MAX 20
#define WIDTH 40
#define LENGTH 20
#define MAX_SCORE 5
#define MAX_SIZE 7
#define MAX_BUFFER 50
#define DIFFICULTY 2

#define WALL 'X'
#define BODY 'O'
#define HEAD '@'
#define FOOD '*'
#define SPIKE '^'
#define SPACE ' '

#define LEFT 's'
#define RIGHT 'f'
#define TOP 'e'
#define BOTTOM 'd'
#define QUIT 'q'

#define SCORE_FILE "score.txt"

#define DEBUG printf("**%d**\n", __LINE__);
#define CLEAR system("clear");


/******************************************/
/*               STRUCTURES               */
/******************************************/

typedef struct PlayerScore PlayerScore;
struct PlayerScore
{
    char *player;
    unsigned int score;
    PlayerScore *next;
};


/******************************************/
/*        PROTOTYPES DES FONCTIONS        */
/******************************************/

PlayerScore *init_player_score();
void init_player_score_v2(PlayerScore **player_score);
void init_head_player_score(PlayerScore **pl, char *name, unsigned int score);

PlayerScore * get_scores_clv(void);
void best_scores_clv(PlayerScore *ps);
int save_score_clv(PlayerScore *ps);

void input_score_clv(PlayerScore **ps, unsigned int p_score);
char *input_player_name_v2();

void insert_node(PlayerScore **pl, char *name, unsigned int score);
void free_players_score(PlayerScore **head);

#endif

