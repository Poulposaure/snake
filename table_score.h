#ifndef TABLE_SCORE
#define TABLE_SCORE



#include <stdio.h>
#include <stdlib.h>
#include <string.h>


/******************************************/
/*               CONSTANTES               */
/******************************************/

#define MAX 20
#define WIDTH 40
#define LENGTH 20
#define MAX_SCORE 5
#define MAX_SIZE 7
#define MAX_BUFFER 50
#define DIFFICULTY 2

#define WALL 'X'
#define BODY 'O'
#define HEAD '@'
#define FOOD '*'
#define SPIKE '^'
#define SPACE ' '

#define LEFT 's'
#define RIGHT 'f'
#define TOP 'e'
#define BOTTOM 'd'
#define QUIT 'q'

#define SCORE_FILE "score.txt"

#define DEBUG printf("**%d**\n", __LINE__);
#define CLEAR system("clear");


/******************************************/
/*               STRUCTURES               */
/******************************************/

typedef struct TableScore
{
    char **players;
    unsigned int number;
    unsigned int scores[MAX_SCORE];
} TableScore;


/******************************************/
/*        PROTOTYPES DES FONCTIONS        */
/******************************************/

void init_table_score(TableScore *table);
void alloc_table_score(TableScore *table);

int get_scores(TableScore *table_s);
void best_scores(TableScore *t);
int save_score(TableScore *table_s);

void input_score(TableScore *table, unsigned int p_score);
void input_player_name(TableScore *table, unsigned int rank);

#endif

