#include "player_score.h"


/*******************************************/
/*        DEFINITIONS DES FONCTIONS        */
/*******************************************/

PlayerScore *init_player_score()
{
    PlayerScore *player_score = (PlayerScore *) malloc(sizeof(PlayerScore));

    player_score->player = "";
    player_score->score = 0;
    player_score->next = NULL;

    return player_score;
}


void init_player_score_v2(PlayerScore **player_score)
{
    *player_score = (PlayerScore *) malloc(sizeof(PlayerScore));

    (*player_score)->player = "";
    (*player_score)->score = 0;
    (*player_score)->next = NULL;

    return;
}


void init_head_player_score(PlayerScore **pl, char *name, unsigned int score)
{
    *pl = (PlayerScore *) malloc(sizeof(PlayerScore));
    (*pl)->next = NULL;
    (*pl)->player = name;
    (*pl)->score = score;

    return;
}


PlayerScore *get_scores_clv(void)
{
    FILE* file = fopen(SCORE_FILE, "r");
    PlayerScore *head = NULL;
    unsigned int score;
    char *name;

    if (file != NULL) {
        while (!(feof(file))) {
            name = (char *) malloc(MAX_BUFFER * sizeof(char));
            fscanf(file, "%s %u\n", name, &score);
            insert_node(&head, name, score);
        }
        fclose(file);

        return (head);
    }
    return (NULL);
}


void best_scores_clv(PlayerScore *ps)
{
    int r;// rank of the player
    PlayerScore *node = NULL;

    getchar();// remove newline from scanf
    system("clear");

    puts("\n\tSCORES\n");
    puts("RANG\tJOUEUR\tPOINTS\n");

    for (r = 0, node = ps; node != NULL; r++)
    {
        printf("%d\t%s\t%d\n", r + 1, node->player, node->score);
        node = node->next;
    }

    printf("\nAppuyez sur entrer pour revenir au menu");
    getchar();

    return;
}


int save_score_clv(PlayerScore *ps)
{
    FILE* file = NULL;
    PlayerScore *node = NULL;
    int i = 0;

    if (ps == NULL) return 0;

    file = fopen(SCORE_FILE, "w");
    if (file != NULL)
    {
        node = ps;

        // we only save 5 scores
        while (node != NULL && i != 5)
        {
            // we write the name and the score of the player
            fprintf(file, "%s %d\n", node->player, node->score);
            node = node->next;
            i++;
        }
        fclose(file);

        return 1;// success, we read the file
    }

    return 0;
}


void insert_node(PlayerScore **pl, char *name, unsigned int score)
{
    PlayerScore *current = *pl, *tmp = *pl;
    PlayerScore *new_node = (PlayerScore *) malloc(sizeof(PlayerScore));

    if (current == NULL)
    {
        init_head_player_score(pl, name, score);

        return;
    }

    new_node->score = score;
    new_node->player = name;
    new_node->next = NULL;

    // we redirect the head to the new node, which is the new head
    if (current->score < score)
    {
        new_node->next = *pl;
        *pl = new_node;

        return;
    }

    // we're adding the node to the end or in-between
    // so we have to iterate till the value of the current node is inferior
    while ((current != NULL) && (current->score > score))
    {
        tmp = current;
        current = current->next;
    }
    // joining with the previous node
    tmp->next = new_node;
    // joining with the next node
    new_node->next = current;

    return;
}


char *input_player_name_v2()
{
    // in case the user input is too big, we use a buffer
    char *name = (char *) malloc(MAX_BUFFER * sizeof(char));
    char *skip_line;

    printf("Entrez votre pseudo : ");
    do
    {
        fgets(name, sizeof(name), stdin);// we get the user input
        skip_line = strchr(name, '\n');// we remove the newline from the input
        *skip_line = '\0';
    } while (strlen(name) >= MAX_SIZE || name[0] == '\0');

    return name;
}


void input_score_clv(PlayerScore **ps, unsigned int p_score)
{
    char *name = input_player_name_v2();
    insert_node(ps, name, p_score);

    return;
}


void free_players_score(PlayerScore **head)
{
    PlayerScore *tmp = *head, *next = NULL;

    while (tmp != NULL)
    {
        next = tmp->next;
        free(tmp);
        tmp = next;
    }

    return;
}

